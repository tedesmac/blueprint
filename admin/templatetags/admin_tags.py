from django import template

from blueprints.models import Blueprint


register = template.Library()


@register.inclusion_tag('blueprints_sidebar.html')
def blueprints():
    all = Blueprint.objects.all().order_by('name')
    return {
        'blueprints': all,
    }
