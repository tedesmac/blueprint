from django.urls import path

import admin.views as views


urlpatterns = [
    path('', views.index, name='blueprint_admin'),
    path('editor/', views.Editor.as_view(), name='bp_pages_new'),
    path('login/', views.blueprint_login, name='bp_login'),
    path('logout/', views.blueprint_logout, name='bp_logout'),
]
