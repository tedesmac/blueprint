from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.http.response import Http404
from django.shortcuts import redirect, render
from django.views.generic import View

from .forms import LoginForm, SiteSettingsForm
from .models import SiteSettings

from blueprints.models import Blueprint
from entries.models import Entry


class AdminView(View):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('bp_login')
        return super().dispatch(request, *args, **kwargs)


class Editor(AdminView):

    def get(self, request):
        return render(request, 'editor.html')


def blueprints(request):
    bps = Blueprint.objects.all().order_by('name')
    return render(request, 'blueprints.html', {
        'blueprints': bps,
        'page_title': 'Blueprints',
    })


def blueprints_add(request):
    return render(request, 'blueprint-editor.html', {
        'page_title': 'New Blueprint'
    })


def blueprints_edit(request, id):
    try:
        bp = Blueprint.objects.get(id=id)
    except Blueprint.DoesNotExist:
        raise Http404
    return render(request, 'blueprint-editor.html', {
        'blueprint': bp,
        'page_title': 'Blueprint editor'
    })


def documents(request):
    if not request.user.is_authenticated:
        return redirect("puzzle_login")

    return render(request, 'documents.html', {
        'page_title': 'Documents',
    })


def entries(request, blueprint_id):
    try:
        blueprint = Blueprint.objects.get(id=blueprint_id)
        page_name = blueprint.name + 's' \
            if blueprint.name[-1] != 's' else blueprint.name
    except Blueprint.DoesNotExists:
        # TODO - Return  404 error
        blueprint = None
        page_name = 'Entries'

    try:
        page = int(request.GET.get('page', 1))
    except KeyError:
        page = 1

    entries = Entry.objects\
                   .filter(blueprint=blueprint)\
                   .order_by('-last_modified')[20*(page-1):20*page]

    if not request.user.is_authenticated:
        return redirect("puzzle_login")

    return render(request, 'entries.html', {
        'entries': entries,
        'page_title': page_name
    })


def images(request):
    if not request.user.is_authenticated:
        return redirect("puzzle_login")

    return render(request, 'images.html', {
        'page_title': 'Images'
    })


def index(request):
    if not request.user.is_authenticated:
        return redirect("blueprint_login")

    return render(request, 'index.html', {
        'page_title': 'Admin'
    })


def blueprint_login(request):
    if request.user.is_authenticated:
        return redirect("blueprint_admin")

    form = LoginForm()

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(to="blueprint_admin")
            else:
                form.add_error(None, 'Invalid username or password.')

    return render(request, 'login.html', {
        'form': form
    })


def blueprint_logout(request):
    logout(request)
    return redirect(settings.LOGOUT_REDIRECT_URL)


def page_editor(request):
    if not request.user.is_authenticated:
        return redirect("puzzle_login")

    return render(request, 'page-editor.html')


def settings_general(request):
    if not request.user.is_authenticated:
        return redirect("puzzle_login")

    if request.method == 'POST':
        form = SiteSettingsForm(request.POST)
        if form.is_valid():
            site_name = form.cleaned_data['site_name']
            site_description = form.cleaned_data['site_description']
            try:
                settings = SiteSettings.objects.get(id=1)
                settings.site_name = site_name
                settings.site_description = site_description
                settings.save()
            except SiteSettings.DoesNotExist:
                SiteSettings.objects.create(site_description=site_description, site_name=site_name)
    else:
        settings = SiteSettings.objects.get_or_create(id=1)[0]
        form = SiteSettingsForm(initial={
            'site_name': settings.site_name,
            'site_description': settings.site_description,
        })
    return render(request, 'settings_general.html', {
        'form': form,
        'page_title': 'General Settings',
    })


def settings_profile(request):
    if not request.user.is_authenticated:
        return redirect("puzzle_login")

    return render(request, 'settings_profile.html', {
        'page_title': 'My Profile'
    })


def settings_users(request):
    if not request.user.is_authenticated:
        return redirect("puzzle_login")

    return render(request, 'settings_users.html', {
        'page_title': 'Users'
    })
