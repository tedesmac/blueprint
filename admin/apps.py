from django.apps import AppConfig


class PuzzleAdminConfig(AppConfig):
    name = 'puzzle_admin'
