from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=32, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'username'}))
    password = forms.CharField(max_length=4096, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'password', 'type': 'password'}))


class SiteSettingsForm(forms.Form):
    site_name = forms.CharField(max_length=32, empty_value='Puzzle',)
    site_description = forms.CharField(max_length=128, required=False,)
