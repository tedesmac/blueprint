from django.db import models


class SiteSettings(models.Model):
    site_name = models.CharField(max_length=32, blank=False, default='Puzzle')
    site_description = models.CharField(max_length=128, blank=True)
