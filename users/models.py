from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import JSONField
from django.db import models


class User(AbstractUser):
    pass


class UserPermissions(models.Model):

    # Can user manage other users.
    invite_users = models.BooleanField(default=False)
    edit_users = models.BooleanField(default=False)
    delete_users = models.BooleanField(default=False)

    # Can user manage pages
    edit_page = models.BooleanField(default=True)
    publish_page = models.BooleanField(default=False)
    delete_page = models.BooleanField(default=False)

    # Can user manage models
    add_model = models.BooleanField(default=False)
    edit_model = models.BooleanField(default=False)
    delete_model = models.BooleanField(default=False)

    # This field stores specific model permissions
    permissions = JSONField
