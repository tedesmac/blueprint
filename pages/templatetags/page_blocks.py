from django import template

import markdown as md

register = template.Library()


def to_css(css: dict) -> str:
    res = ''
    for key, value in css.items():
        res += '{0}: {1}; '.format(key, value)
    return res


@register.inclusion_tag(filename='blocks/audio.html')
def audio(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/button.html')
def button(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/code.html')
def code(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/container.html')
def container(block: dict) -> dict:
    blocks = block['blocks']
    style = to_css(block.get('style', {}))
    return {
        'style': style,
        'blocks': blocks,
    }


@register.inclusion_tag(filename='blocks/cover.html')
def cover(block: dict) -> dict:
    href = block.get('value', '')
    style = to_css(block.get('style', {})) + 'background-image: url(' + href + '); '
    return {
        'style': style,
    }


@register.inclusion_tag(filename='blocks/embed.html')
def embed(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/file.html')
def file(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/gallery.html')
def gallery(block) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/image.html')
def image(block: dict) -> dict:
    style = to_css(block.get('style', {}))
    return {
        'style': style
    }


@register.inclusion_tag(filename='blocks/line.html')
def line(block: dict) -> dict:
    style = to_css(block.get('style', {}))
    return {
        'style': style
    }


@register.inclusion_tag(filename='blocks/list.html')
def list_block(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/map.html')
def map_block(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/markdown.html')
def markdown(block: dict) -> dict:
    html = md.markdown(block.get('value', ''))
    style = to_css(block.get('style', {}))
    return {
        'html': html,
        'style': style
    }


@register.inclusion_tag(filename='blocks/menu.html')
def menu(block: dict) -> dict:

    class MenuItem:
        def __init__(self, href: str, name: str):
            self.href = href
            self.name = name

    items = [MenuItem(href=item['href'], name=item['name']) for item in block['items']]

    link_style = to_css(block.get('link_style',  {}))

    style = to_css(block.get('style', {}))

    return {
        'items': items,
        'link_style': link_style,
        'style': style
    }


@register.inclusion_tag(filename='blocks/page.html')
def page(blocks: dict) -> dict:
    return {
        'blocks': blocks
    }


@register.inclusion_tag(filename='blocks/pageblock.html')
def pageblock(block: dict) -> dict:
    type = block['type'].name
    return {
        'block': block,
        'type': type,
    }


@register.inclusion_tag(filename='blocks/quote.html')
def quote(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/raw_html.html')
def raw_html(block: dict) -> dict:
    html = block.get('value', '')
    return {
        'html': html
    }


@register.inclusion_tag(filename='blocks/script.html')
def script(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/spacer.html')
def spacer(block: dict) -> dict:
    style = to_css(block.get('style', {}))
    return {
        'style': style
    }


@register.inclusion_tag(filename='blocks/string.html')
def string(block: dict) -> dict:
    style = to_css(block.get('style', {}))
    value = block.get('value', '')
    return {
        'style': style,
        'value': value
    }


@register.inclusion_tag(filename='blocks/text.html')
def text(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/timeline.html')
def timeline(block: dict) -> dict:
    return {}


@register.inclusion_tag(filename='blocks/video.html')
def video(block) -> dict:
    return {}
