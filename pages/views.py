from django.shortcuts import render
from django.http.response import Http404

from .block_types import BlockTypes

from pages.models import Page


def data2view(view: dict, data: dict) -> dict:
    res = {**view}

    if res['type'] == BlockTypes.CONTAINER:
        res['blocks'] = [data2view(block, data) for block in res['blocks']]
    else:
        if 'for' in view:
            res['value'] = data[view['for']]

    return res


post_model = {
    'id': 0,
    'site': 0,
    'name': 'Post',
    'blocks': {
        'title': {
            'type': BlockTypes.STRING,
            'default': 'Title',
        },
        'cover': {
            'type': BlockTypes.COVER,
            'default': ''
        },
        'audio': {
            'type': BlockTypes.RAW_HTML,
            'default': ''
        },
        'content': {
            'type': BlockTypes.MARKDOWN,
            'default': ''
        }
    }
}


post_view_preview = {
    'type': BlockTypes.CONTAINER,
    'style': {
        'flex-direction': 'column',
        'width': '100%',
        'margin': '1em',
    },
    'blocks': [
        {
            'for': 'title',
            'type': BlockTypes.STRING,
            'value': '',
            'style': {
                'font-size': '1.5em',
                'font-weight': 'bold',
                'margin': '0.5em'
            }
        },
        {
            'for': 'cover',
            'type': BlockTypes.COVER,
            'value': '',
            'style': {
                'width': '100%',
                'height': '60vh',
            }
        },
        {
            'for': 'audio',
            'type': BlockTypes.RAW_HTML,
            'value': ''
        },
        {
            'for': 'content',
            'type': BlockTypes.MARKDOWN,
            'value': '',
            'style': {}
        },
    ]
}


post_entries = [
    {
        'title': 'Lorem ipsum dolor sit amet',
        'cover': 'https://i.imgur.com/MUZMWnG.jpg',
        'audio': '<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/534506589&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>',
        'content': '''Lorem markdownum aptae nullo et a Hodites quaerit sidera soror virilibus:
cladibus me sinus dedit. Fontemque ad corpus precor tremulis afuit, admonitu
Cadme consilioque adsim cruento ingratos relinquunt Dixit! Pontum dexterior
probabit captat aures, pande nulla sola putes capillos, de cadunt, pulcherrima.

1. Tellure et Aeeta putares mentem
2. Et turbam sortemque de ullis
3. Troezen centauri exorabilis Antiphatae manus
4. Pariterque latuere

Ipse arte fixus Phoebo acuti heros clam artem arentis quisque. Concubitus gradu
lacessit gravi: spatioque suam. Cum et, nova apri quasque deposcunt uti at liber
Hyperione frater. Et caput gigantas novissima nequiquam Crantoris est exemit
Ulixes require est mitem.

+ Devorat pedibusque scitabere dextra novissima iuveni solus dummodo furoribus,
+ convertit mox Caesar erat. Evaserat comites optavit edocuit mentis ille vanum
+ montis: suarum fecissem. Bracchia quae. Tamen etiam Telamonque movere atris
+ honore virgineusque modo habent, celebrant per hortus proceres tamen.''',
    },
    {
        'title': 'Lorem ipsum dolor sit amet',
        'cover': 'https://i.imgur.com/MUZMWnG.jpg',
        'audio': '<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/534506589&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>',
        'content': '''Lorem markdownum aptae nullo et a Hodites quaerit sidera soror virilibus:
cladibus me sinus dedit. Fontemque ad corpus precor tremulis afuit, admonitu
Cadme consilioque adsim cruento ingratos relinquunt Dixit! Pontum dexterior
probabit captat aures, pande nulla sola putes capillos, de cadunt, pulcherrima.

1. Tellure et Aeeta putares mentem
2. Et turbam sortemque de ullis
3. Troezen centauri exorabilis Antiphatae manus
4. Pariterque latuere

Ipse arte fixus Phoebo acuti heros clam artem arentis quisque. Concubitus gradu
lacessit gravi: spatioque suam. Cum et, nova apri quasque deposcunt uti at liber
Hyperione frater. Et caput gigantas novissima nequiquam Crantoris est exemit
Ulixes require est mitem.

+ Devorat pedibusque scitabere dextra novissima iuveni solus dummodo furoribus,
+ convertit mox Caesar erat. Evaserat comites optavit edocuit mentis ille vanum
+ montis: suarum fecissem. Bracchia quae. Tamen etiam Telamonque movere atris
+ honore virgineusque modo habent, celebrant per hortus proceres tamen.''',
    },
    {
        'title': 'Lorem ipsum dolor sit amet',
        'cover': 'https://i.imgur.com/MUZMWnG.jpg',
        'audio': '<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/534506589&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>',
        'content': '''Lorem markdownum aptae nullo et a Hodites quaerit sidera soror virilibus:
cladibus me sinus dedit. Fontemque ad corpus precor tremulis afuit, admonitu
Cadme consilioque adsim cruento ingratos relinquunt Dixit! Pontum dexterior
probabit captat aures, pande nulla sola putes capillos, de cadunt, pulcherrima.

1. Tellure et Aeeta putares mentem
2. Et turbam sortemque de ullis
3. Troezen centauri exorabilis Antiphatae manus
4. Pariterque latuere

Ipse arte fixus Phoebo acuti heros clam artem arentis quisque. Concubitus gradu
lacessit gravi: spatioque suam. Cum et, nova apri quasque deposcunt uti at liber
Hyperione frater. Et caput gigantas novissima nequiquam Crantoris est exemit
Ulixes require est mitem.

+ Devorat pedibusque scitabere dextra novissima iuveni solus dummodo furoribus,
+ convertit mox Caesar erat. Evaserat comites optavit edocuit mentis ille vanum
+ montis: suarum fecissem. Bracchia quae. Tamen etiam Telamonque movere atris
+ honore virgineusque modo habent, celebrant per hortus proceres tamen.''',
    },
]


site_settings = {
    'name': 'A X I O M A',
    'slogan': 'M A T E R I A   D E D U C T I V A',
}


menu = {
    'levels': 1,
    'items': [
        {
            'name': 'CONECTES',
            'href': 'https://old.reddit.com'
        },
        {
            'name': 'PROGRAMAS',
            'href': 'https://old.reddit.com'
        },
        {
            'name': 'LECTURAS',
            'href': 'https://old.reddit.com'
        },
        {
            'name': 'NOSOTROS',
            'href': 'https://old.reddit.com'
        },
        {
            'name': 'CONTACTO',
            'href': 'https://old.reddit.com'
        },
    ]
}


page_blocks = {
    'blocks': [
        {
            'type': BlockTypes.CONTAINER,
            'style': {
                'flex-direction': 'column'
            },
            'blocks': [
                {
                    'type': BlockTypes.IMAGE,
                    'style': {
                        'background-image': 'url(/static/img/axioma_logo.jpg)',
                        'width': '80vw',
                        'height': '20vh',
                        'margin': '1em',
                    },
                },
                {
                    'type': BlockTypes.MENU,
                    'items': menu['items'],
                    'link_style': {
                        'color': 'black',
                        'margin': '0.5em'
                    },
                    'style': {
                        'width': '80vw',
                        'justify-content': 'space-evenly',
                        'margin': '1em'
                    },
                },
                {
                    'type': BlockTypes.LINE,
                    'style': {
                        'width': '100%',
                        'height': '2px',
                        'background': 'black'
                    }
                }
            ]
        },
        {
            'type': BlockTypes.CONTAINER,
            'style': {
                'flex-direction': 'column',
                'width': '80vw',
                'margin': '2em',
            },
            'blocks': [data2view(post_view_preview, block) for block in post_entries]
        },
    ]
}


def home(request):
    return page(request, '')


def page(request, route):
    try:
        page = Page.objects.get(route=route)
    except Page.DoesNotExist:
        pass
        # raise Http404

    return render(request, 'page.html', {
        'name': 'Inicio',
        'siteName': site_settings['name'],
        'slogan': site_settings['slogan'],
        'blocks': page_blocks['blocks']
    })
