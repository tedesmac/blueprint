from enum import Enum


def forDjango(cls):
    cls.do_not_call_in_template = True
    return cls


@forDjango
class BlockTypes(Enum):
    AUDIO = 'AUDIO'
    BUTTON = 'BUTTON'
    CODE = 'CODE'
    CONTAINER = 'CONTAINER'
    COVER = 'COVER'
    EMBED = 'EMBED'
    FILE = 'FILE'
    GALLERY = 'GALLERY'
    IMAGE = 'IMAGE'
    LINE = 'LINE'
    LIST = 'LIST'
    MAP = 'MAP'
    MARKDOWN = 'MARKDOWN'
    MENU = 'MENU'
    PAGE = 'PAGE'
    QUOTE = 'QUOTE'
    RAW_HTML = 'RAW_HTML'
    SCRIPT = 'SCRIPT'
    SPACER = 'SPACER'
    STRING = 'STRING'
    TABLE = 'TABLE'
    TEXT = 'TEXT'
    TIMELINE = 'TIMELINE'
    VIDEO = 'VIDEO'
