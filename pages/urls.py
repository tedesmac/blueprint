from django.urls import path, re_path

import pages.views as views


urlpatterns = [
    path('', views.home),
    re_path(r'^(?P<route>.*)/$', views.page),
]
