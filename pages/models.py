from django.db import models
from django.utils.text import slugify

from utils.models import BaseModel


class Page(BaseModel):
    description = models.CharField(max_length=256, blank=True)
    in_menu = models.BooleanField(default=False)
    parent = models.ForeignKey(
        'self',
        related_name='children',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    route = models.CharField(unique=True, blank=True, max_length=1024)
    slug = models.SlugField()

    def get_route(self) -> str:
        route = self.slug + '/'

        if self.parent:
            parent = Page.objects.get(self.parent)
            route = parent.get_route() + route

        return route

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        self.route = self.get_route()
        super().save(*args, **kwargs)
