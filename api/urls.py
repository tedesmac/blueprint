from django.urls import path

from api.views.blueprints import (
    BlueprintDetail,
    BlueprintList
)
from api.views.entries import (
    EntryDetail,
    EntryList
)
from api.views.pages import (
    PageDetail,
    PageList
)
from api.views.users import (
    UserDetail,
    UserList,
    UserThis
)


urlpatterns = [
    path('blueprints/', BlueprintList.as_view()),
    path('blueprints/<int:id>/', BlueprintDetail.as_view()),
    path('entries/', EntryList.as_view()),
    path('entries/<int:id>/', EntryDetail.as_view()),
    path('pages/', PageList.as_view()),
    path('pages/<int:id>/', PageDetail.as_view()),
    path('users/', UserList.as_view()),
    path('users/<int:id>/', UserDetail.as_view()),
    path('users/this/', UserThis.as_view()),
]
