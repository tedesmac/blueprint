from rest_framework import serializers

from views.models import View


class ViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = View
        read_only_fields = ('id', 'date_created', 'date_modified')
        fields = '__all__'
