from rest_framework import serializers

from entries.models import Entry


class EntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Entry
        read_only_fields = ('id', 'date_created', 'date_modified')
        fields = '__all__'
