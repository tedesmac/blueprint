from rest_framework import serializers

from pages.models import Page


class PageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Page
        read_only_fields = ('id', 'created_by',
                            'date_created', 'date_modified')
        fields = '__all__'
