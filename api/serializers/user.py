from rest_framework import serializers

from users.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model= User
        read_only_fields = (
            'id',
            'last_login',
            'is_superuser',
            'is_staff',
            'is_active',
            'date_joined'
        )
        exclude = ('password',)
