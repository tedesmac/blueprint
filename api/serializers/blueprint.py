from rest_framework import serializers

from blueprints.models import Blueprint


class BlueprintSerializer(serializers.ModelSerializer):

    class Meta:
        model = Blueprint
        read_only_fields = ('id', 'created_by',
                            'date_created', 'date_modified')
        fields = '__all__'
