from django.db.models import Model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.views import APIView as BaseAPIView


class APIView(BaseAPIView):

    def __init__(self, model, serializer):
        if not issubclass(model, Model):
            raise Exception(
                'model must be a subclass of {}'.format(Model)
            )
        if not issubclass(serializer, Serializer):
            raise Exception(
                'serializer must be a subclass of {}'.format(Serializer)
            )

        self.model = model
        self.serializer = serializer


class APIDetailView(APIView):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def delete(self, request, id):
        try:
            obj = self.model.objects.get(id=id)
        except self.model.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND
            )
        obj.delete()
        return Response(
            status=status.HTTP_204_NO_CONTENT
        )

    def get(self, request, id):
        try:
            obj = self.model.objects.get(id=id)
        except self.model.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND
            )
        serializer = self.serializer(obj)
        return Response(serializer.data)

    def put(self, request, id):
        try:
            obj = self.model.objects.get(id=id)
        except self.model.DoesNotExist:
            return Response(
                status=status.HTTP_404_NOT_FOUND
            )
        serializer = self.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class APIListView(APIView):

    class Meta:
        order_by = ['id']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, request):
        objects = self.model.objects.all().order_by(*self.Meta.order_by)
        serializer = self.serializer(objects, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )
