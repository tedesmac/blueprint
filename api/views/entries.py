from rest_framework.response import Response

from api.views.api_views import APIDetailView, APIListView
from api.serializers.entry import EntrySerializer
from entries.models import Entry


class EntryDetail(APIDetailView):

    def __init__(self):
        super().__init__(model=Entry, serializer=EntrySerializer)


class EntryList(APIListView):

    class Meta:
        order_by = ('id', 'date_modified')

    def __init__(self):
        super().__init__(model=Entry, serializer=EntrySerializer)

    def get(self, request):
        blueprint = int(request.GET.get('blueprint', 0))
        entries = None
        if blueprint:
            entries = self.model.objects.filter(blueprint=blueprint)
        else:
            entries = self.model.objects.all()
        entries = entries.order_by(*self.Meta.order_by)
        serializer = self.serializer(entries, many=True)
        return Response(serializer.data)
