from api.serializers.page import PageSerializer
from api.views.api_views import APIDetailView, APIListView
from pages.models import Page


class PageDetail(APIDetailView):

    def __init__(self):
        super().__init__(model=Page, serializer=PageSerializer)


class PageList(APIListView):

    class Meta:
        order_by = ('id', 'date_modified')

    def __init__(self):
        super().__init__(model=Page, serializer=PageSerializer)
