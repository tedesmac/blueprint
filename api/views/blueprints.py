from api.serializers.blueprint import BlueprintSerializer
from api.views.api_views import APIDetailView, APIListView
from blueprints.models import Blueprint


class BlueprintDetail(APIDetailView):

    def __init__(self):
        super().__init__(model=Blueprint, serializer=BlueprintSerializer)


class BlueprintList(APIListView):

    class Meta:
        order_by = ['name', 'id']

    def __init__(self):
        super().__init__(model=Blueprint, serializer=BlueprintSerializer)
