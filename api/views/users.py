from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers.user import UserSerializer
from api.views.api_views import (
    APIDetailView,
    APIListView
)
from users.models import User


class UserDetail(APIDetailView):

    def __init__(self):
        super().__init__(model=User, serializer=UserSerializer)


class UserList(APIListView):

    def __init__(self):
        super().__init__(model=User, serializer=UserSerializer)


class UserThis(APIView):

    def get(self, request):
        user = request.user
        serializer = UserSerializer(user)
        return Response(serializer.data)
