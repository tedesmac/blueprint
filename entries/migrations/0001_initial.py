# Generated by Django 2.1.3 on 2019-03-20 15:37

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blueprints', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', django.contrib.postgres.fields.jsonb.JSONField(default=dict)),
                ('date_created', models.DateTimeField(auto_now=True)),
                ('date_modified', models.DateTimeField()),
                ('name', models.CharField(max_length=50)),
                ('blueprint', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='blueprints.Blueprint')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
