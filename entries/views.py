from django.shortcuts import get_object_or_404, render
from django.views.generic.base import View

from entries.models import Entry


class EntryView(View):

    def get(self, request, id):
        entry = get_object_or_404(Entry, id=id)
        return render(request, 'entry.html', {
            "entry": entry
        })
