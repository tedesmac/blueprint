from django.db import models

from blueprints.models import Blueprint
from utils.models import BaseModel


class Entry(BaseModel):
    blueprint = models.ForeignKey(
        Blueprint,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
