from django.urls import path

import entries.views as views


urlpatterns = [
    path('<int:id>/', views.EntryView.as_view())
]
