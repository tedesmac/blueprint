**Blueprint** is a new generation CMS that allows you to create websites an custom content types
using a drag & drop editor. Not coding skill required.

# Requirements

+ Nginx
+ PostgreSQL
+ Python 3.6

### Note

Blueprint is still at an early stage of development and is not in a usable state.