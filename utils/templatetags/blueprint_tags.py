from django import template

register = template.Library()


@register.simple_tag
def page_description() -> str:
    from admin.models import SiteSettings

    res = ''

    try:
        settings = SiteSettings.objects.get(id=1)
        res = settings.site_description
    except SiteSettings.DoesNotExist:
        pass

    return res


@register.simple_tag
def site_name() -> str:
    from admin.models import SiteSettings

    res = ''

    try:
        settings = SiteSettings.objects.get(id=1)
        res = settings.site_name
    except SiteSettings.DoesNotExist:
        pass

    return res
