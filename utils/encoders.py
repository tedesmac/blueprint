import json
from pages.block_types import BlockTypes


class JSONEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, BlockTypes):
            return obj.value
        return json.JSONEncoder.default(self, obj)
