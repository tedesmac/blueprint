import json
from rest_framework import serializers
from utils.encoders import JSONEncoder


class DataField(serializers.Field):

    def to_internal_value(self, data):
        return json.dumps(data, cls=JSONEncoder)

    def to_representation(self, data):
        try:
            d = json.loads(data)
        except (json.JSONDecodeError, TypeError):
            d = {}
        return d
