from datetime import datetime, timezone
from django.contrib.postgres.fields import JSONField
from django.db import models
import json

from users.models import User


class BaseModel(models.Model):

    class Meta:
        abstract = True

    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    data = JSONField(default=dict, blank=False, null=False)
    date_created = models.DateTimeField(null=True, default=None)
    date_modified = models.DateTimeField()
    name = models.CharField(max_length=50, blank=False, null=False)

    @property
    def blocks(self) -> list:
        try:
            return self.data['blocks']
        except KeyError:
            return []

    def save(self, *args, **kwargs):
        if self.date_created is None:
            self.date_created = datetime.now(tz=timezone.utc)
        self.date_modified = datetime.now(tz=timezone.utc)
        super().save(*args, **kwargs)
