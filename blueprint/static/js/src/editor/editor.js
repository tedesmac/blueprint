import Vue from "vue";
import Vuex from "vuex";
import Editor from "./components/editor";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: { blocks: [] },

  mutations: {
    addBlock(state, payload) {
      state.blocks = [...state.blocks, payload.block];
    }
  }
});

new Vue({
  el: "#editor",
  store,
  components: { Editor },
  template: "<Editor />"
});
