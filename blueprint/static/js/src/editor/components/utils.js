import BlockTypes from './block_types'


const blockConstructor = (type) => {
    let res = {
        block_id: uuid(8),
        type: type,
        style: {},
        id: '',
        classes: ''
    }

    if (type === BlockTypes.COLUMN || type === BlockTypes.ROW) res = {...res, data: []}
    else res = {...res, data: ''}

    return res
}


const uuid = (size) => {

    const getChar = () => {
        const val = Math.floor(Math.random() * 36)
        return val < 26 ? String.fromCharCode(val + 97) : String.fromCharCode(val + 22)
    }

    let res = ''

    for (let i = 0; i < size; i++) res += getChar()

    return res

}


export { blockConstructor }