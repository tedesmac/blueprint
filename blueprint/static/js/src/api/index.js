import axios from "axios";

axios.defaults.xsrfHeaderName = "X-CSRFToken";
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.withCredentials = true;

const URI = "/bp/api";

const delete_ = (uri, id) => {
  return axios
    .delete(`${uri}/${id}`)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return Promise.reject(error);
    });
};

const get = (uri, id = null, query = {}) => {
  if (id) {
    return axios
      .get(`${uri}/${id}/`, { params: query })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }
  return axios
    .get(`${uri}/`, { params: query })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return Promise.reject(error);
    });
};

const post = (uri, data) => {
  return axios
    .post(uri, data)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return Promise.reject(error);
    });
};

const put = (uri, id, data) => {
  return axios
    .put(`${uri}/${id}/`, data)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return Promise.reject(error);
    });
};

const this_ = uri => {
  return axios
    .get(`${uri}/this/`)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return Promise.reject(error);
    });
};

export default {
  blueprints: {
    delete: id => delete_(`${URI}/blueprints`, id),
    get: (id = null, query = {}) => get(`${URI}/blueprints`, id, query),
    post: data => post(`${URI}/blueprints`, data),
    put: (id, data) => put(`${URI}/blueprints`, id, data)
  },

  entries: {
    delete: id => delete_(`${URI}/entries`, id),
    get: (id = null, query = {}) => get(`${URI}/entries`, id, query),
    post: data => post(`${URI}/entries`, data),
    put: (id, data) => put(`${URI}/entries`, id, data)
  },

  pages: {
    delete: id => delete_(`${URI}/pages`, id),
    get: (id = null, query = {}) => get(`${URI}/pages`, id, query),
    post: data => post(`${URI}/pages`, data),
    put: (id, data) => put(`${URI}/pages`, id, data)
  },

  sites: {
    this: () => this_(`${URI}/sites`)
  },

  users: {
    get: (id = null, query = {}) => get(`${URI}/users`, id, query),
    put: (id, data) => put(`${URI}/users`, id, data),
    this: () => this_(`${URI}/users`)
  },

  views: {
    delete: id => delete_(`${URI}/views`, id),
    get: (id = null, query = {}) => get(`${URI}/views`, id, query),
    post: data => post(`${URI}/views`, data),
    put: (id, data) => put(`${URI}/views`, id, data)
  }
};
