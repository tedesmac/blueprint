import vue from "vue";
import Pages from "./components/pages";

new vue({
  el: "#pages",
  components: { Pages },
  template: "<Pages />"
});
