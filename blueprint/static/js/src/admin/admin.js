import axios from "axios";
import Vue from "vue";
import VueRouter from "vue-router";
import { sync } from "vuex-router-sync";
import Vuex from "vuex";
import Admin from "./components/admin";
import routes from "./routes";

axios.defaults.xsrfHeaderName = "X-CSRFToken";
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.withCredentials = true;

Vue.use(VueRouter);
Vue.use(Vuex);

const router = new VueRouter({ routes });

const store = new Vuex.Store({
  state: {
    blueprints: [],
    user: null
  },

  getters: {
    blocks: state => index => {
      try {
        const blueprint = state.blueprints[index];
        const blocks = blueprint.blocks;
        if (blocks !== undefined) {
          return blocks;
        }
        return [];
      } catch (error) {
        console.log("Error getting blocks:", error);
        return [];
      }
    },
    blueprints: state => state.blueprints,
    username: state => {
      if (state.user !== null) {
        return state.user.username;
      }
      return "";
    }
  },

  mutations: {
    fetchBlueprints(state) {
      axios
        .get("/bp/api/blueprints")
        .then(response => {
          state.blueprints = response.data;
        })
        .catch(error => {
          console.log("Error while fetching blueprints:", error);
        });
    },
    fetchUser(state) {
      axios
        .get("/bp/api/users/this/")
        .then(response => {
          state.user = response.data;
        })
        .catch(error => {
          console.log("Error while fetching user:", error);
        });
    }
  }
});

sync(store, router);

new Vue({
  el: "#admin",
  router: router,
  store: store,
  components: { Admin },
  template: "<Admin />"
});
