import Blueprints from "./components/blueprints";
import BlueprintEditor from "./components/blueprint-editor/editor";
import Entries from "./components/entries";
import EntryEditor from "./components/entry-editor/editor";
import Pages from "./components/pages";

const routes = [
  {
    path: "/blueprints",
    component: Blueprints
  },
  {
    path: "/blueprints/edit",
    component: BlueprintEditor,
    props: route => ({
      action: "action" in route.query ? route.query.action.toLowerCase() : "",
      id: Number(route.query.id)
    })
  },
  {
    path: "/entries/:blueprint(\\d+)",
    component: Entries,
    props: route => ({
      id: Number(route.params.blueprint)
    })
  },
  {
    path: "/entries/:blueprint(\\d+)/edit",
    component: EntryEditor,
    props: route => ({
      action: "action" in route.query ? route.query.action.toLowerCase() : "",
      bp_id: Number(route.params.blueprint),
      id: Number(route.query.id)
    })
  },
  {
    path: "/pages",
    component: Pages
  }
];

export default routes;
