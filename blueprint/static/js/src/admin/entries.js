import vue from "vue";
import Entries from "./components/entries";

new vue({
  el: "#entries",
  components: { Entries },
  template: "<Entries />"
});
