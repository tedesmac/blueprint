import vue from "vue";
import Blueprints from "./components/blueprints";

new vue({
  el: "#blueprints",
  components: { Blueprints },
  template: "<Blueprints />"
});
