const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
  entry: {
    admin: path.resolve(__dirname, "src/admin/admin.js"),
    editor: path.resolve(__dirname, "src/editor/editor.js")
  },
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "[name].js"
  },
  resolve: {
    alias: {
      api: path.resolve(__dirname, "src/api"),
      vue$: "vue/dist/vue.esm.js"
    },
    extensions: [".js", ".json", ".vue"]
  },
  /*optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },*/
  plugins: [new VueLoaderPlugin()],
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        use: "vue-loader"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
  mode: "development"
};
